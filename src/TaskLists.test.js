import React from 'react'
import ReactDOM from 'react-dom'
import TaskLists from './TaskLists'

it('renders without crashing', () => {
	const div = document.createElement('div')
	ReactDOM.render(<TaskLists />, div)
	ReactDOM.unmountComponentAtNode(div)
})
