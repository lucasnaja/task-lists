import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import DeleteIcon from '@material-ui/icons/Delete'
import Icon from '@material-ui/core/Icon'
import Grid from '@material-ui/core/Grid'

const styles = theme => ({
	root: {
		display: 'flex',
	},
	appBar: {
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
	},
	appBarShift: {
		width: '100%',
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
	},
	drawerHeader: {
		display: 'flex',
		alignItems: 'center',
		padding: '0 8px',
		...theme.mixins.toolbar,
		justifyContent: 'flex-end',
	},
	rightIcon: {
		marginLeft: theme.spacing.unit,
	},
	marginTop: {
		marginTop: 10
	},
})

class TaskLists extends Component {
	constructor(props) {
		super(props)

		this.addTask = this.addTask.bind(this)
		this.deleteSelected = this.deleteSelected.bind(this)
		this.changeName = this.changeName.bind(this)
		this.changeDescription = this.changeDescription.bind(this)
	}

	state = {
		taskLists: [],
		name: '',
		description: ''
	}

	addTask() {
		if (this.state.name !== '' && this.state.description !== '') {
			this.setState({
				taskLists: [
					...this.state.taskLists, {
						name: this.state.name,
						description: this.state.description
					}
				],
				name: '',
				description: ''
			})
		} else {
			alert('Please, add a name and a description to the task.')
		}
	}

	deleteSelected(task) {
		this.setState({
			taskLists: this.state.taskLists.filter(actTask => task !== actTask)
		})
	}

	changeName(e) {
		this.setState({
			name: e.target.value
		})
	}

	changeDescription(e) {
		this.setState({
			description: e.target.value
		})
	}

	render() {
		const { classes } = this.props
		const { taskLists } = this.state

		return (
			<div className={classes.root}>
				<CssBaseline />
				<AppBar
					position="fixed"
					className={classes.appBar}
				>
					<Toolbar>
						<Typography variant="h6" color="inherit" noWrap>
							Task Lists - React
						</Typography>
					</Toolbar>
				</AppBar>

				<main style={{ width: '100%', padding: '0 10px' }}>
					<div className={classes.drawerHeader} />

					<Grid
						container
						spacing={8}
					>
						<Grid item xs={12} sm={4}>
							<TextField
								fullWidth={true}
								onChange={this.changeName}
								label="Name"
								value={this.state.name}
								margin="normal"
								variant="outlined"
							/>
						</Grid>

						<Grid item xs={12} sm={8}>
							<TextField
								fullWidth={true}
								onChange={this.changeDescription}
								label="Description"
								value={this.state.description}
								margin="normal"
								variant="outlined"
							/>
						</Grid>
					</Grid>

					<Divider />

					<Button className={classes.marginTop} onClick={this.addTask} variant="contained">
						Add
						<Icon className={classes.rightIcon}>add_circle_ou</Icon>
					</Button>

					{
						taskLists.map((task, index) =>
							<Card className={classes.marginTop} key={index}>
								<CardContent>
									<Typography variant="h5" className={classes.title} color="textSecondary" gutterBottom>
										Title: {task.name}
									</Typography>
									<Typography component="p">
										Description: {task.description}
									</Typography>
								</CardContent>

								<CardActions>
									<Button onClick={() => this.deleteSelected(task)} variant="contained">
										Delete
        								<DeleteIcon className={classes.rightIcon} />
									</Button>
								</CardActions>
							</Card>
						)
					}
				</main>
			</div>
		)
	}
}

TaskLists.propTypes = {
	classes: PropTypes.object.isRequired,
	theme: PropTypes.object.isRequired,
}

export default withStyles(styles, { withTheme: true })(TaskLists)
